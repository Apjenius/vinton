package Model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Model.Util.AR;
import Model.Util.BillingHistory;
import Model.Util.UBDetailsList;
import Model.Util.WaterCons;
import Utils.ParcelableUtils;

/**
 * Created by Saraschandraa on 22-03-2015.
 */
public class AccountDetailResponse implements Parcelable {

    ArrayList<AR> arList;
    ArrayList<BillingHistory> billingHistoryList;
    UBDetailsList ubDetailsList;
    ArrayList<WaterCons> waterConsList;

    public AccountDetailResponse(JSONObject jsonObject){
        try{
            arList = new ArrayList<>();
            JSONObject json = jsonObject.optJSONObject("AR");
            if(json == null){
                JSONArray array = jsonObject.getJSONArray("AR");
                for(int i=0; i<array.length(); i++){
                    JSONObject jObj = (JSONObject) array.get(i);
                    AR data = new AR(jObj);
                    arList.add(data);
                }
            }else{
                AR data = new AR(json);
                arList.add(data);
            }
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            billingHistoryList = new ArrayList<>();
            JSONObject json = jsonObject.optJSONObject("BillingHistory");
            if(json == null){
                JSONArray array = jsonObject.getJSONArray("BillingHistory");
                for(int i=0; i<array.length(); i++){
                    JSONObject jObj = (JSONObject) array.get(i);
                    BillingHistory data = new BillingHistory(jObj);
                    billingHistoryList.add(data);
                }
            }else{
                BillingHistory data = new BillingHistory(json);
                billingHistoryList.add(data);
            }
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            waterConsList = new ArrayList<>();
            JSONObject json = jsonObject.optJSONObject("WaterCons");
            if(json == null){
                JSONArray array = jsonObject.getJSONArray("WaterCons");
                for(int i=0; i<array.length(); i++){
                    JSONObject jObj = (JSONObject) array.get(i);
                    WaterCons data = new WaterCons(jObj);
                    waterConsList.add(data);
                }
            }else{
                WaterCons data = new  WaterCons(json);
                waterConsList.add(data);
            }
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            JSONObject jObj = jsonObject.getJSONObject("UBDetailsList");
            ubDetailsList = new UBDetailsList(jObj);
        }catch(JSONException e){
            e.printStackTrace();
        }
    }

    public ArrayList<AR> getArList() {
        return arList;
    }

    public void setArList(ArrayList<AR> arList) {
        this.arList = arList;
    }

    public ArrayList<BillingHistory> getBillingHistoryList() {
        return billingHistoryList;
    }

    public void setBillingHistoryList(ArrayList<BillingHistory> billingHistoryList) {
        this.billingHistoryList = billingHistoryList;
    }

    public UBDetailsList getUbDetailsList() {
        return ubDetailsList;
    }

    public void setUbDetailsList(UBDetailsList ubDetailsList) {
        this.ubDetailsList = ubDetailsList;
    }

    public ArrayList<WaterCons> getWaterConsList() {
        return waterConsList;
    }

    public void setWaterConsList(ArrayList<WaterCons> waterConsList) {
        this.waterConsList = waterConsList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        ParcelableUtils.write(dest, arList, flags);
        ParcelableUtils.write(dest, billingHistoryList, flags);
        ParcelableUtils.write(dest, ubDetailsList, flags);
        ParcelableUtils.write(dest, waterConsList, flags);
    }

    public AccountDetailResponse(Parcel in){
        arList = ParcelableUtils.readParcelableList(in, AR.class.getClassLoader());
        billingHistoryList = ParcelableUtils.readParcelableList(in, BillingHistory.class.getClassLoader());
        ubDetailsList = ParcelableUtils.readParcelable(in, UBDetailsList.class.getClassLoader());
        waterConsList = ParcelableUtils.readParcelableList(in, WaterCons.class.getClassLoader());
    }

    public static Creator<AccountDetailResponse> CREATOR = new Creator<AccountDetailResponse>() {
        @Override
        public AccountDetailResponse createFromParcel(Parcel source) {
            return new AccountDetailResponse(source);
        }

        @Override
        public AccountDetailResponse[] newArray(int size) {
            return new AccountDetailResponse[size];
        }
    };
}
