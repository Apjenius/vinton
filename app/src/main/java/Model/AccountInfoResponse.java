package Model;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Model.Util.Accounts;
import Utils.ParcelableUtils;

/**
 * Created by Saraschandraa on 23-03-2015.
 */
public class AccountInfoResponse implements Parcelable {

    ArrayList<Accounts> accountList;

    public AccountInfoResponse(JSONObject jsonObject){
        try{
            accountList = new ArrayList<>();
            JSONObject json = jsonObject.optJSONObject("Accounts");
            if(json == null){
                JSONArray array = jsonObject.getJSONArray("Accounts");
                for (int i =0; i<array.length(); i++){
                    JSONObject jObj = (JSONObject) array.get(i);
                    Accounts data = new Accounts(jObj);
                    accountList.add(data);
                }
            }else{
                Accounts data = new Accounts(json);
                accountList.add(data);
            }
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    public ArrayList<Accounts> getAccountList() {
        return accountList;
    }

    public void setAccountList(ArrayList<Accounts> accountList) {
        this.accountList = accountList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        ParcelableUtils.write(dest, accountList, flags);
    }

    public AccountInfoResponse(Parcel in){
        accountList = ParcelableUtils.readParcelableList(in, Accounts.class.getClassLoader());
    }

    public static Creator<AccountInfoResponse> CREATOR = new Creator<AccountInfoResponse>() {
        @Override
        public AccountInfoResponse createFromParcel(Parcel source) {
            return new AccountInfoResponse(source);
        }

        @Override
        public AccountInfoResponse[] newArray(int size) {
            return new AccountInfoResponse[size];
        }
    };
}
