package Model.Util;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Saraschandraa on 22-03-2015.
 */
public class PayType {

    String xml;

    public PayType (JSONObject jsonObject){

        try{
            xml = jsonObject.getString("xml:space");
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    public String getXml() {
        return xml;
    }

    public void setXml(String xml) {
        this.xml = xml;
    }
}
