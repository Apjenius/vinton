package Model.Util;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import Utils.ParcelableUtils;

/**
 * Created by Saraschandraa on 22-03-2015.
 */
public class BillingHistory implements Parcelable {

    String Amount;
    String BillNo;
    PayType payType;
    String RunningBalance;
    String TransDate;
    String TransType;

    public BillingHistory(JSONObject jsonObject){
        try{
            Amount = jsonObject.getString("Amount");
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            BillNo = jsonObject.getString("BillNo");
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            RunningBalance = jsonObject.getString("RunningBalance");
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            TransDate = jsonObject.getString("TransDate");
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            TransType = jsonObject.getString("TransType");
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            JSONObject jObj  = jsonObject.getJSONObject("PayType");
            payType = new PayType(jObj);
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    public String getBillNo() {
        return BillNo;
    }

    public void setBillNo(String billNo) {
        BillNo = billNo;
    }

    public PayType getPayType() {
        return payType;
    }

    public void setPayType(PayType payType) {
        this.payType = payType;
    }

    public String getRunningBalance() {
        return RunningBalance;
    }

    public void setRunningBalance(String runningBalance) {
        RunningBalance = runningBalance;
    }

    public String getTransDate() {
        return TransDate;
    }

    public void setTransDate(String transDate) {
        TransDate = transDate;
    }

    public String getTransType() {
        return TransType;
    }

    public void setTransType(String transType) {
        TransType = transType;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        ParcelableUtils.write(dest, Amount);
        ParcelableUtils.write(dest, BillNo);
        ParcelableUtils.write(dest, RunningBalance);
        ParcelableUtils.write(dest, TransDate);
        ParcelableUtils.write(dest, TransType);
    }

    public BillingHistory(Parcel in){
        Amount = ParcelableUtils.readString(in);
        BillNo = ParcelableUtils.readString(in);
        RunningBalance = ParcelableUtils.readString(in);
        TransDate = ParcelableUtils.readString(in);
        TransType = ParcelableUtils.readString(in);
    }

    public static Creator<BillingHistory> CREATOR = new Creator<BillingHistory>() {
        @Override
        public BillingHistory createFromParcel(Parcel source) {
            return new BillingHistory(source);
        }

        @Override
        public BillingHistory[] newArray(int size) {
            return new BillingHistory[size];
        }
    };
}
