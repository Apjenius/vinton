package Model.Util;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import Utils.ParcelableUtils;

/**
 * Created by Saraschandraa on 22-03-2015.
 */
public class Accounts implements Parcelable{

    String AccountAddress;
    String AccountNumber;



    public Accounts(JSONObject jsonObject){
        try{
            AccountAddress = jsonObject.getString("AccountAddress");
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            AccountNumber = jsonObject.getString("AccountNumber");
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    public String getAccountAddress() {
        return AccountAddress;
    }

    public void setAccountAddress(String accountAddress) {
        AccountAddress = accountAddress;
    }

    public String getAccountNumber() {
        return AccountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        AccountNumber = accountNumber;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override

    public void writeToParcel(Parcel dest, int flags) {
        ParcelableUtils.write(dest, AccountAddress);
        ParcelableUtils.write(dest, AccountNumber);
    }

    public Accounts(Parcel in){
        AccountAddress = ParcelableUtils.readString(in);
        AccountNumber = ParcelableUtils.readString(in);
    }

    public static Creator<Accounts> CREATOR = new Creator<Accounts>() {
        @Override
        public Accounts createFromParcel(Parcel source) {
            return new Accounts(source);
        }

        @Override
        public Accounts[] newArray(int size) {
            return new Accounts[size];
        }
    };
}
