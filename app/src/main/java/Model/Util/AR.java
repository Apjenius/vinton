package Model.Util;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import Utils.ParcelableUtils;

/**
 * Created by Saraschandraa on 22-03-2015.
 */
public class AR  implements Parcelable{

    String AR;
    String Value;

    public AR(JSONObject jsonObject){
        try{
            AR = jsonObject.getString("AR");
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            Value = jsonObject.getString("Value");
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    public String getAR() {
        return AR;
    }

    public void setAR(String AR) {
        this.AR = AR;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        ParcelableUtils.write(dest, AR);
        ParcelableUtils.write(dest, Value);
    }

    public AR(Parcel in){
        AR = ParcelableUtils.readString(in);
        Value = ParcelableUtils.readString(in);
    }

    public static Creator<AR> CREATOR = new Creator<Model.Util.AR>() {
        @Override
        public AR createFromParcel(Parcel source) {
            return new AR(source);
        }

        @Override
        public AR[] newArray(int size) {
            return new AR[size];
        }
    };
}
