package Model.Util;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;

import Utils.ParcelableUtils;

/**
 * Created by Saraschandraa on 22-03-2015.
 */
public class WaterCons implements Parcelable{

    String Amount;
    String Cons;
    String Date;
    String MeterCode;
    String MeterNumber;
    String Reading;
    String Type;

    public WaterCons(JSONObject jsonObject){
        try{
            Amount = jsonObject.getString("Amount");
        }catch (JSONException e){
            e.printStackTrace();
        }
        try{
            Cons = jsonObject.getString("Cons");
        }catch (JSONException e){
            e.printStackTrace();
        }
        try{
            Date = jsonObject.getString("Date");
        }catch (JSONException e){
            e.printStackTrace();
        }
        try{
            MeterCode = jsonObject.getString("MeterCode");
        }catch (JSONException e){
            e.printStackTrace();
        }
        try{
            MeterNumber = jsonObject.getString("MeterNumber");
        }catch (JSONException e){
            e.printStackTrace();
        }
        try{
            Reading = jsonObject.getString("Reading");
        }catch (JSONException e){
            e.printStackTrace();
        }
        try{
            Type = jsonObject.getString("Type");
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    public String getCons() {
        return Cons;
    }

    public void setCons(String cons) {
        Cons = cons;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getMeterCode() {
        return MeterCode;
    }

    public void setMeterCode(String meterCode) {
        MeterCode = meterCode;
    }

    public String getMeterNumber() {
        return MeterNumber;
    }

    public void setMeterNumber(String meterNumber) {
        MeterNumber = meterNumber;
    }

    public String getReading() {
        return Reading;
    }

    public void setReading(String reading) {
        Reading = reading;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        ParcelableUtils.write(dest, Amount);
        ParcelableUtils.write(dest, Cons);
        ParcelableUtils.write(dest, Date);
        ParcelableUtils.write(dest, MeterCode);
        ParcelableUtils.write(dest, MeterNumber);
        ParcelableUtils.write(dest, Reading);
        ParcelableUtils.write(dest, Type);
    }

    public WaterCons(Parcel in){
        Amount = ParcelableUtils.readString(in);
        Cons = ParcelableUtils.readString(in);
        Date = ParcelableUtils.readString(in);
        MeterCode = ParcelableUtils.readString(in);
        MeterNumber = ParcelableUtils.readString(in);
        Reading = ParcelableUtils.readString(in);
        Type = ParcelableUtils.readString(in);
    }

    public static Creator<WaterCons> CREATOR = new Creator<WaterCons>() {
        @Override
        public WaterCons createFromParcel(Parcel source) {
            return new WaterCons(source);
        }

        @Override
        public WaterCons[] newArray(int size) {
            return new WaterCons[size];
        }
    };
}
