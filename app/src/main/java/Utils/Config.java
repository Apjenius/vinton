package Utils;

import java.util.ArrayList;

import Model.AccountDetailResponse;
import Model.AccountInfoResponse;
import Model.Util.Accounts;

/**
 * Created by Saraschandraa on 22-03-2015.
 */
public class Config {


    static String WSCALL = "http://mobiledata.visualgov.com/UtilityAPIVN/UtilDetail/";

    public static String GETACCOUNTTAB = WSCALL + "GetAccounts";
    public static String GETACCOUNTINFO = WSCALL + "GetUBAccountInfo";

    public static ArrayList<Accounts> accounts;
    public static AccountInfoResponse accountInfoResponse;
    public static ArrayList<AccountDetailResponse> accounDetailList = new ArrayList<>();
    public static ArrayList<AccountDetailResponse> selectPaymentList = new ArrayList<>();
    public static ArrayList<Accounts> selectedAccountList = new ArrayList<>();
}
