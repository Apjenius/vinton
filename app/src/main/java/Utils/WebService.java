package Utils;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import Model.AccountDetailResponse;
import Model.AccountInfoResponse;
import Model.Util.Accounts;
import apjenius.vinton.HomeFragmentActivity;
import apjenius.vinton.SplashActivity;

/**
 * Created by Saraschandraa on 20-03-2015.
 */
public class WebService {

    Context context;
    AccountDetailResponse accountDetailResponse;
    JSONObject jsonResponse, response;
    WSLoadingDialog loadingDialog;
    GetWSDetails getWSDetails;
    List<NameValuePair> list;
    String WSType;
    Accounts accounts;


    public WebService(Context context, String type, List<NameValuePair> list) {

        this.context = context;
        this.list = list;
        WSType = type;
        loadingDialog = new WSLoadingDialog(context);

        excuteWS(list, WSType,0);

    }

    public void excuteWS(List<NameValuePair> list, String type, int position) {
        getWSDetails = new GetWSDetails(list, type, position);
        getWSDetails.execute();
    }


    public class GetWSDetails extends AsyncTask<Void, Void, Void> {

        List<NameValuePair> list;
        String result;
        String WSType;
        int position;

        public GetWSDetails(List<NameValuePair> list, String WSType, int position) {
            this.list = list;
            this.WSType = WSType;
            this.position = position;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadingDialog.showWSLoadingDialog();
        }

        @Override
        protected Void doInBackground(Void... params) {

            HTTPHandler handler = new HTTPHandler(context);
            list.add(new BasicNameValuePair("VGToken", "VGVN983719"));
            list.add(new BasicNameValuePair("UserName", "jschulze@dtventures.net"));
            list.add(new BasicNameValuePair("Password", "password"));
            list.add(new BasicNameValuePair("Ticket", "SDSFSDFSD"));

            switch (WSType) {
                case "AccountTabs":
                    result = handler.doPost(Config.GETACCOUNTTAB, list);
                    break;

                case "AccountDetails":
                    result = handler.doPost(Config.GETACCOUNTINFO, list);
                    break;

                default:
                    break;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            loadingDialog.hideWSLoadingDialog();
            if (result != null) {

                switch (WSType) {

                    case "AccountTabs":
                        Log.i("ACCOUNTTAB RESPONSE", result);
                        try {
                            jsonResponse = new JSONObject(result);
                            response = new JSONObject();
                            response = jsonResponse.getJSONObject("Util");
                            Config.accountInfoResponse = new AccountInfoResponse(response);
                            Config.accounts = Config.accountInfoResponse.getAccountList();
                            for(int i=0; i<Config.accountInfoResponse.getAccountList().size(); i++){
                                accounts = Config.accountInfoResponse.getAccountList().get(i);
                                list = new ArrayList<>();
                                list.add(new BasicNameValuePair("UBAccount", accounts.getAccountNumber()));
                                getWSDetails = new GetWSDetails(list, "AccountDetails", i);
                                getWSDetails.execute();
                                Toast.makeText(context, "Called"+i, Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case "AccountDetails":
                        Log.i("ACCOUNTDETAILS RESPONSE", result);
                        try {
                            jsonResponse = new JSONObject(result);
                            response = new JSONObject();
                            response = jsonResponse.getJSONObject("Util");
                            accountDetailResponse = new AccountDetailResponse(response);
                            Config.accounDetailList.add(accountDetailResponse);
                            if(position == Config.accounts.size() - 1) {
                                SplashActivity.defaultInstantance().finishLoading();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    default:
                        break;
                }
            }
        }
    }


}
