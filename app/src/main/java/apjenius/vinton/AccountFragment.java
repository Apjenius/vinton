package apjenius.vinton;

import android.annotation.TargetApi;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

import Adapter.BillingHistoryAdapter;
import Adapter.ConsumptionHistoryAdapter;
import Model.AccountDetailResponse;
import Model.AccountInfoResponse;
import Model.Util.Accounts;
import Utils.Config;
import Utils.WebService;
import widgets.PopoverView;

/**
 * Created by Saraschandraa on 14-03-2015.
 */
public class AccountFragment extends Fragment implements View.OnClickListener, PopoverView.PopoverViewDelegate {

    View accountView;
    LinearLayout mTab1Layout, mTab2Layout, mTab3Layout, mTab4Layout, mTab5Layout, mTabHolder;
    TextView mTabText1, mTabText2, mTabText3, mTabText4, mTabText5, mTvBtnPayNow,
            mTvAccountNo, mTvCustomerName, mTvPreviousBalance, mTvPTDPayments, mTvAmountDue;
    RecyclerView mRcConsumptionHistoryList, mRcBillingHistoryList;
    ImageView mIvInfoDetail, mIvBillingHistoryDetail, mIvConsumptionHistoryDetail;
    DialogClass mDialog;
    public AccountDetailResponse accountDetails;
    public Accounts accountTab;
    public ArrayList<AccountDetailResponse> accountDetailsList;
    public ArrayList<Accounts> accountTabList;
    List<NameValuePair> list;
    public BillingHistoryAdapter billingHistoryAdapter;
    public ConsumptionHistoryAdapter consumptionHistoryAdapter;
    WebService ws;
    int position = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        accountView = inflater.inflate(R.layout.fragment_accountinfo, container, false);

        mTabText1 = (TextView) accountView.findViewById(R.id.tv_tab1_text);
        mTabText2 = (TextView) accountView.findViewById(R.id.tv_tab2_text);
        mTabText3 = (TextView) accountView.findViewById(R.id.tv_tab3_text);
        mTabText4 = (TextView) accountView.findViewById(R.id.tv_tab4_text);
        mTabText5 = (TextView) accountView.findViewById(R.id.tv_tab5_text);
        mTvAccountNo = (TextView) accountView.findViewById(R.id.tv_acc_no);
        mTvCustomerName = (TextView) accountView.findViewById(R.id.tv_cust_name);
        mTvPreviousBalance = (TextView) accountView.findViewById(R.id.tv_acc_previousbal);
        mTvPTDPayments = (TextView) accountView.findViewById(R.id.tv_acc_ptdpayments);
        mTvAmountDue = (TextView) accountView.findViewById(R.id.tv_amount_due);
        mTvBtnPayNow = (TextView) accountView.findViewById(R.id.tv_paynow);


        mTab1Layout = (LinearLayout) accountView.findViewById(R.id.ll_tab1_layout);
        mTab2Layout = (LinearLayout) accountView.findViewById(R.id.ll_tab2_layout);
        mTab3Layout = (LinearLayout) accountView.findViewById(R.id.ll_tab3_layout);
        mTab4Layout = (LinearLayout) accountView.findViewById(R.id.ll_tab4_layout);
        mTab5Layout = (LinearLayout) accountView.findViewById(R.id.ll_tab5_layout);
        mTabHolder = (LinearLayout) accountView.findViewById(R.id.ll_tab_holder);

        mTab1Layout.setOnClickListener(this);
        mTab2Layout.setOnClickListener(this);
        mTab3Layout.setOnClickListener(this);
        mTab4Layout.setOnClickListener(this);
        mTab5Layout.setOnClickListener(this);
        mTvBtnPayNow.setOnClickListener(this);

        mRcConsumptionHistoryList = (RecyclerView) accountView.findViewById(R.id.rc_consumptionhistory);
        mRcBillingHistoryList = (RecyclerView) accountView.findViewById(R.id.rc_billinghistory);

        mRcConsumptionHistoryList.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRcBillingHistoryList.setLayoutManager(new LinearLayoutManager(getActivity()));

        mIvInfoDetail = (ImageView) accountView.findViewById(R.id.iv_infodetail);
        mIvBillingHistoryDetail = (ImageView) accountView.findViewById(R.id.iv_billhistorydetail);
        mIvConsumptionHistoryDetail = (ImageView) accountView.findViewById(R.id.iv_consHistorydetail);

        mIvInfoDetail.setOnClickListener(this);
        mIvBillingHistoryDetail.setOnClickListener(this);
        mIvConsumptionHistoryDetail.setOnClickListener(this);

        list = new ArrayList<>();


        getAccountDetailResponse(position);

        return accountView;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_tab1_layout:
                mTab1Layout.setBackground(getActivity().getResources().getDrawable(R.drawable.tab_selector_left));
                mTab2Layout.setBackground(getActivity().getResources().getDrawable(R.drawable.tab_unselected_straight));
                mTab3Layout.setBackground(getActivity().getResources().getDrawable(R.drawable.tab_unselected_straight));
                mTab4Layout.setBackground(getActivity().getResources().getDrawable(R.drawable.tab_unselected_straight));
                mTab5Layout.setBackground(getActivity().getResources().getDrawable(R.drawable.tab_unselected_right));

                mTabText1.setTextColor(getResources().getColor(R.color.white));
                mTabText2.setTextColor(getResources().getColor(R.color.border_red));
                mTabText3.setTextColor(getResources().getColor(R.color.border_red));
                mTabText4.setTextColor(getResources().getColor(R.color.border_red));
                mTabText5.setTextColor(getResources().getColor(R.color.border_red));
                position = 0;
                getAccountDetailResponse(position);


                break;
            case R.id.ll_tab2_layout:
                mTab1Layout.setBackground(getActivity().getResources().getDrawable(R.drawable.tab_unselected_left));
                mTab2Layout.setBackground(getActivity().getResources().getDrawable(R.drawable.tab_selected_straight));
                mTab3Layout.setBackground(getActivity().getResources().getDrawable(R.drawable.tab_unselected_straight));
                mTab4Layout.setBackground(getActivity().getResources().getDrawable(R.drawable.tab_unselected_straight));
                mTab5Layout.setBackground(getActivity().getResources().getDrawable(R.drawable.tab_unselected_right));

                mTabText1.setTextColor(getResources().getColor(R.color.border_red));
                mTabText2.setTextColor(getResources().getColor(R.color.white));
                mTabText3.setTextColor(getResources().getColor(R.color.border_red));
                mTabText4.setTextColor(getResources().getColor(R.color.border_red));
                mTabText5.setTextColor(getResources().getColor(R.color.border_red));
                position = 1;
                getAccountDetailResponse(position);

                break;
            case R.id.ll_tab3_layout:
                mTab1Layout.setBackground(getActivity().getResources().getDrawable(R.drawable.tab_unselected_left));
                mTab2Layout.setBackground(getActivity().getResources().getDrawable(R.drawable.tab_unselected_straight));
                mTab3Layout.setBackground(getActivity().getResources().getDrawable(R.drawable.tab_selected_straight));
                mTab4Layout.setBackground(getActivity().getResources().getDrawable(R.drawable.tab_unselected_straight));
                mTab5Layout.setBackground(getActivity().getResources().getDrawable(R.drawable.tab_unselected_right));

                mTabText1.setTextColor(getResources().getColor(R.color.border_red));
                mTabText2.setTextColor(getResources().getColor(R.color.border_red));
                mTabText3.setTextColor(getResources().getColor(R.color.white));
                mTabText4.setTextColor(getResources().getColor(R.color.border_red));
                mTabText5.setTextColor(getResources().getColor(R.color.border_red));

                position = 2;
                getAccountDetailResponse(position);

                break;
            case R.id.ll_tab4_layout:
                mTab1Layout.setBackground(getActivity().getResources().getDrawable(R.drawable.tab_unselected_left));
                mTab2Layout.setBackground(getActivity().getResources().getDrawable(R.drawable.tab_unselected_straight));
                mTab3Layout.setBackground(getActivity().getResources().getDrawable(R.drawable.tab_unselected_straight));
                mTab4Layout.setBackground(getActivity().getResources().getDrawable(R.drawable.tab_selected_straight));
                mTab5Layout.setBackground(getActivity().getResources().getDrawable(R.drawable.tab_unselected_right));

                mTabText1.setTextColor(getResources().getColor(R.color.border_red));
                mTabText2.setTextColor(getResources().getColor(R.color.border_red));
                mTabText3.setTextColor(getResources().getColor(R.color.border_red));
                mTabText4.setTextColor(getResources().getColor(R.color.white));
                mTabText5.setTextColor(getResources().getColor(R.color.border_red));

                position = 3;
                getAccountDetailResponse(position);
                break;
            case R.id.ll_tab5_layout:
                mTab1Layout.setBackground(getActivity().getResources().getDrawable(R.drawable.tab_unselected_left));
                mTab2Layout.setBackground(getActivity().getResources().getDrawable(R.drawable.tab_unselected_straight));
                mTab3Layout.setBackground(getActivity().getResources().getDrawable(R.drawable.tab_unselected_straight));
                mTab4Layout.setBackground(getActivity().getResources().getDrawable(R.drawable.tab_unselected_straight));
                mTab5Layout.setBackground(getActivity().getResources().getDrawable(R.drawable.tab_selector_right));

                mTabText1.setTextColor(getResources().getColor(R.color.border_red));
                mTabText2.setTextColor(getResources().getColor(R.color.border_red));
                mTabText3.setTextColor(getResources().getColor(R.color.border_red));
                mTabText4.setTextColor(getResources().getColor(R.color.border_red));
                mTabText5.setTextColor(getResources().getColor(R.color.white));

                RelativeLayout rootView = (RelativeLayout)getActivity().findViewById(R.id.rl_accountroot);

                PopoverView popoverView = new PopoverView(getActivity(), R.layout.cell_accountsmore);
                popoverView.setContentSizeForViewInPopover(new Point(320, 340));
                popoverView.setDelegate(this);
                popoverView.showPopoverFromRectInViewGroup(HomeFragmentActivity.defaultInstantance().rootLayout, PopoverView.getFrameForView(v), PopoverView.PopoverArrowDirectionUp, true);
                break;

            case R.id.iv_infodetail:
                mDialog = new DialogClass(getActivity(), "AccountDetail");
//                mDialog.showAccountInfoDetail("AccountDetail");
                break;

            case R.id.iv_billhistorydetail:
                mDialog = new DialogClass(getActivity(), "BillingHistory");
//                mDialog.showAccountInfoDetail("BillingHistory");
                break;

            case R.id.iv_consHistorydetail:
                mDialog = new DialogClass(getActivity(), "ConsumptionHistory");
//                mDialog.showAccountInfoDetail("ConsumptionHistory");
                break;

            case R.id.tv_paynow:
                accountDetailsList = new ArrayList<>();
                accountTabList = new ArrayList<>();
                accountDetailsList.add(accountDetails);
                accountTabList.add(accountTab);
                Bundle args = new Bundle();
                args.putInt("AccountsSize", 1);
                args.putInt("Position", position);
                args.putParcelableArrayList("AccountDetails", accountDetailsList);
                args.putParcelableArrayList("AccountTab", accountTabList);
                HomeFragmentActivity.defaultInstantance().changeFragment(HomeFragmentActivity.Fragments.PAYMENT_FRAGMENT, args);
                break;
            default:
                break;
        }
    }

    public void getAccountDetailResponse(int position) {
        if(Config.accounDetailList != null && Config.accounDetailList.size() != 0) {
            accountDetails = Config.accounDetailList.get(position);
            accountTab = Config.accounts.get(position);
            mTabHolder.setVisibility(View.VISIBLE);
            setAccountTabs();
            mTvCustomerName.setText(accountDetails.getUbDetailsList().getName());
            mTvAccountNo.setText(accountTab.getAccountNumber());
            mTvPTDPayments.setText(accountDetails.getUbDetailsList().getPTDPayments());
            mTvPreviousBalance.setText(accountDetails.getUbDetailsList().getPreviousBalance());
            mTvAmountDue.setText("$" + accountDetails.getUbDetailsList().getAmountDue());

            billingHistoryAdapter = new BillingHistoryAdapter(getActivity(), accountDetails.getBillingHistoryList());
            mRcBillingHistoryList.setAdapter(billingHistoryAdapter);

            consumptionHistoryAdapter = new ConsumptionHistoryAdapter(getActivity(), accountDetails.getWaterConsList());
            mRcConsumptionHistoryList.setAdapter(consumptionHistoryAdapter);
        }


    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void setAccountTabs() {
        switch (Config.accountInfoResponse.getAccountList().size()) {
            case 1:
                mTab1Layout.setBackground(getActivity().getResources().getDrawable(R.drawable.tab_border_single_selected));
                mTab2Layout.setVisibility(View.GONE);
                mTab3Layout.setVisibility(View.GONE);
                mTab4Layout.setVisibility(View.GONE);
                mTab5Layout.setVisibility(View.GONE);

                mTabText1.setText(Config.accountInfoResponse.getAccountList().get(0).getAccountNumber());
                break;

            case 2:
                mTab3Layout.setVisibility(View.GONE);
                mTab4Layout.setVisibility(View.GONE);

                mTabText1.setText(Config.accountInfoResponse.getAccountList().get(0).getAccountNumber());
                mTabText2.setText(Config.accountInfoResponse.getAccountList().get(1).getAccountNumber());
                mTabText5.setText(getActivity().getResources().getString(R.string.acc_pay_all));
                break;

            case 3:
                mTab4Layout.setVisibility(View.GONE);
                mTabText1.setText(Config.accountInfoResponse.getAccountList().get(0).getAccountNumber());
                mTabText2.setText(Config.accountInfoResponse.getAccountList().get(1).getAccountNumber());
                mTabText3.setText(Config.accountInfoResponse.getAccountList().get(2).getAccountNumber());
                mTabText5.setText(getActivity().getResources().getString(R.string.acc_pay_all));
                break;

            case 4:
                mTabText1.setText(Config.accountInfoResponse.getAccountList().get(0).getAccountNumber());
                mTabText2.setText(Config.accountInfoResponse.getAccountList().get(1).getAccountNumber());
                mTabText3.setText(Config.accountInfoResponse.getAccountList().get(2).getAccountNumber());
                mTabText4.setText(Config.accountInfoResponse.getAccountList().get(3).getAccountNumber());
                mTabText5.setText(getActivity().getResources().getString(R.string.acc_pay_all));
                break;

        }
    }

    @Override
    public void popoverViewWillShow(PopoverView view) {

    }

    @Override
    public void popoverViewDidShow(PopoverView view) {

    }

    @Override
    public void popoverViewWillDismiss(PopoverView view) {

    }

    @Override
    public void popoverViewDidDismiss(PopoverView view) {

    }
}