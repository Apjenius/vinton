package apjenius.vinton;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import Utils.Config;

/**
 * Created by Saraschandraa on 14-03-2015.
 */
public class NavigationDrawerFragment extends Fragment implements View.OnClickListener {

    View drawerView;
    DrawerLayout mDrawerLayout;
    ActionBarDrawerToggle mDrawerToggle;
    RelativeLayout mRlUBAccLayout, mRlPaymentLayout, mRlAutoPayLayout;
    TextView mTvUBAccount, mTvMakePayment, mTvAutoPay;
    DialogClass mDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        drawerView = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
        mRlUBAccLayout = (RelativeLayout) drawerView.findViewById(R.id.rl_accountlayout);
        mRlPaymentLayout = (RelativeLayout) drawerView.findViewById(R.id.rl_mkpaymentlayout);
        mRlAutoPayLayout = (RelativeLayout) drawerView.findViewById(R.id.rl_autppaylayout);


        mTvUBAccount = (TextView) drawerView.findViewById(R.id.tv_ubaccount);
        mTvMakePayment = (TextView) drawerView.findViewById(R.id.tv_mkpayment);
        mTvAutoPay = (TextView) drawerView.findViewById(R.id.tv_autopay);

        mRlPaymentLayout.setOnClickListener(this);
        mRlUBAccLayout.setOnClickListener(this);
        mRlAutoPayLayout.setOnClickListener(this);
        return drawerView;
    }

    public void showDrawer(DrawerLayout drawerLayout, Toolbar toolbar) {
        mDrawerLayout = drawerLayout;
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                getActivity().invalidateOptionsMenu();
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                getActivity().invalidateOptionsMenu();
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                if(HomeFragmentActivity.defaultInstantance().viewingFragment == HomeFragmentActivity.Fragments.ACCOUNT_FRAGMENT){
                    setNavDrawerColor(0);
                }else if(HomeFragmentActivity.defaultInstantance().viewingFragment == HomeFragmentActivity.Fragments.PAYMENT_FRAGMENT){
                    setNavDrawerColor(1);
                }else if(HomeFragmentActivity.defaultInstantance().viewingFragment == HomeFragmentActivity.Fragments.AUTOPAY_FRAGMENT){
                    setNavDrawerColor(2);
                }
            }

        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_accountlayout:
                HomeFragmentActivity.defaultInstantance().changeFragment(HomeFragmentActivity.Fragments.ACCOUNT_FRAGMENT, null);
                setNavDrawerColor(0);
                mDrawerLayout.closeDrawer(Gravity.LEFT);
                break;

            case R.id.rl_mkpaymentlayout:
                mDrawerLayout.closeDrawer(Gravity.LEFT);
                setNavDrawerColor(1);
                mDialog = new DialogClass(getActivity(),"PaymentListDetail");
                break;

            case R.id.rl_autppaylayout:
                mDrawerLayout.closeDrawer(Gravity.LEFT);
                setNavDrawerColor(2);
                mDialog = new DialogClass(getActivity(), "AutoPayDisclaimer");
                break;

            default:
                break;
        }
    }

    private void setNavDrawerColor(int menu){
        switch(menu){
            case 0:
                mTvUBAccount.setTextColor(getResources().getColor(R.color.menu_ubacc_selected));
                mTvMakePayment.setTextColor(getResources().getColor(R.color.menu_unselected));
                mTvAutoPay.setTextColor(getResources().getColor(R.color.menu_unselected));
                break;
            case 1:
                mTvUBAccount.setTextColor(getActivity().getResources().getColor(R.color.menu_unselected));
                mTvMakePayment.setTextColor(getActivity().getResources().getColor(R.color.menu_payment_selected));
                mTvAutoPay.setTextColor(getResources().getColor(R.color.menu_unselected));
                break;
            case 2:
                mTvUBAccount.setTextColor(getActivity().getResources().getColor(R.color.menu_unselected));
                mTvMakePayment.setTextColor(getActivity().getResources().getColor(R.color.menu_unselected));
                mTvAutoPay.setTextColor(getResources().getColor(R.color.menu_autopay_selected));
                break;

        }
    }
}
