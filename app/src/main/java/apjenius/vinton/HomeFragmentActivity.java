package apjenius.vinton;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import java.util.ArrayList;

/**
 * Created by Saraschandraa on 14-03-2015.
 */
public class HomeFragmentActivity extends ActionBarActivity {

    public static HomeFragmentActivity rootInstance;
    public static HomeFragmentActivity defaultInstantance(){
        return  rootInstance;
    }

    public RelativeLayout rootLayout;

    public enum Fragments{
        ACCOUNT_FRAGMENT, PAYMENT_FRAGMENT, AUTOPAY_FRAGMENT;
    }

    public AccountFragment accountFragment;
    public PaymentFragment paymentFragment;
    public AutoPayFragment autoPayFragment;

    Toolbar mToolbar;
    NavigationDrawerFragment mNavigationDrawerFragment;
    FragmentTransaction mFragmentTransaction;
    public ArrayList<Fragments> fragmentsList;
    public Fragments viewingFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homefragment);
        rootInstance = this;

        mToolbar = (Toolbar) findViewById(R.id.tl_toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        mNavigationDrawerFragment =  (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.fr_navdrawer);
        mNavigationDrawerFragment.showDrawer((DrawerLayout) findViewById(R.id.dw_drawerLayout), mToolbar);

        rootLayout = (RelativeLayout) findViewById(R.id.fl_root_holder);

        fragmentsList = new ArrayList<>();

        changeFragment(Fragments.ACCOUNT_FRAGMENT, null);
    }


    /**
     * Method called to change to which Fragment
     * @param fragments the current fragment to be displayed.
     * @param args the arguments to be passed from one fragment to another.
     */
    public void changeFragment(Fragments fragments, Bundle args){

        mFragmentTransaction = getSupportFragmentManager().beginTransaction();

        if (fragmentsList != null) {
            fragmentsList.add(fragments);
        }
        if (fragmentsList.size() != 0) {
            viewingFragment = fragmentsList.get(fragmentsList.size() - 1);
        }
        Log.i("FRAGMENT LIST", "" + fragmentsList.toString());
        Log.i("VIEWING FRAGMENT", "" + viewingFragment);

        switch (fragments){
            case ACCOUNT_FRAGMENT:
                if(accountFragment == null){
                    accountFragment = new AccountFragment();
                }
                try{
                    accountFragment.setArguments(args);
                }catch (IllegalStateException e){
                    e.printStackTrace();
                }
                mFragmentTransaction.replace(R.id.fl_root_holder, accountFragment);
                mFragmentTransaction.commit();
                break;

            case PAYMENT_FRAGMENT:
                if(paymentFragment == null){
                    paymentFragment = new PaymentFragment();
                }
                try{
                    paymentFragment.setArguments(args);
                }catch (IllegalStateException e){
                    e.printStackTrace();
                }
                mFragmentTransaction.replace(R.id.fl_root_holder, paymentFragment);
                mFragmentTransaction.addToBackStack(null);
                mFragmentTransaction.commit();
                break;

            case AUTOPAY_FRAGMENT:
                if(autoPayFragment == null){
                    autoPayFragment = new AutoPayFragment();
                }
                try{
                    autoPayFragment.setArguments(args);
                }catch (IllegalStateException e){
                    e.printStackTrace();
                }
                mFragmentTransaction.replace(R.id.fl_root_holder, autoPayFragment);
                mFragmentTransaction.addToBackStack(null);
                mFragmentTransaction.commit();
            default:
                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (viewingFragment != Fragments.ACCOUNT_FRAGMENT && fragmentsList.size() != 0) {
            fragmentsList.remove(fragmentsList.size() - 1);
        }
        if (fragmentsList.size() != 0) {
            viewingFragment = fragmentsList.get(fragmentsList.size() - 1);
        }
    }
}
