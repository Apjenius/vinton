package apjenius.vinton;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import apjenius.vinton.R;
import apjenius.vinton.fragments.PaymentOverViewFragment;
import apjenius.vinton.fragments.StepFourFragment;
import apjenius.vinton.fragments.StepOneFragment;
import apjenius.vinton.fragments.StepThreeFragment;
import apjenius.vinton.fragments.StepTwoFragment;

/**
 * Created by Saraschandraa on 12-04-2015.
 */
public class AutoPayFragment extends Fragment {

    View autopayView;

    FragmentTransaction fragmentTransaction;

    public StepTwoFragment stepTwoFragment;
    public StepThreeFragment stepThreeFragment;
    public StepFourFragment stepFourFragment;
    public PaymentOverViewFragment paymentOverViewFragment;

    public enum ChildFragments{
        STEP_TWO, STEP_THREE, STEP_FOUR, STEP_PAYOVERVIEW
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        autopayView = inflater.inflate(R.layout.fragment_autopay, container, false);

        changeChildFragments(ChildFragments.STEP_TWO, null);
        return autopayView;
    }


    public void changeChildFragments(ChildFragments childFragments, Bundle args){
        fragmentTransaction = getChildFragmentManager().beginTransaction();
        switch (childFragments){
            case STEP_TWO:
                stepTwoFragment = new StepTwoFragment();
                stepTwoFragment.FragmentType(1);
                try{
                    stepTwoFragment.setArguments(args);
                }catch (IllegalStateException e){
                    e.printStackTrace();
                }
                fragmentTransaction.replace(R.id.fl_autopay_holder, stepTwoFragment);
                fragmentTransaction.commit();
                break;

            case STEP_THREE:
                stepThreeFragment = new StepThreeFragment();
                stepThreeFragment.FragmentType(1);
                try{
                    stepThreeFragment.setArguments(args);
                }catch (IllegalStateException e){
                    e.printStackTrace();
                }
                fragmentTransaction.replace(R.id.fl_autopay_holder, stepThreeFragment);
                fragmentTransaction.commit();
                break;

            case STEP_FOUR:
                stepFourFragment = new StepFourFragment();
                try{
                    stepFourFragment.setArguments(args);
                }catch (IllegalStateException e){
                    e.printStackTrace();
                }
                fragmentTransaction.replace(R.id.fl_autopay_holder, stepFourFragment);
                fragmentTransaction.commit();
                break;

            case STEP_PAYOVERVIEW:
                paymentOverViewFragment = new PaymentOverViewFragment();
                try{
                    paymentOverViewFragment.setArguments(args);
                }catch (IllegalStateException e){
                    e.printStackTrace();
                }
                fragmentTransaction.replace(R.id.fl_autopay_holder, paymentOverViewFragment);
                fragmentTransaction.commit();
                break;

        }
    }
}

