package apjenius.vinton;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.apache.http.NameValuePair;

import java.util.ArrayList;
import java.util.List;

import Utils.Config;
import Utils.WSLoadingDialog;
import Utils.WebService;
import WebService.UserAccountInfoWS;


public class SplashActivity extends ActionBarActivity {

    LinearLayout mTabSignin, mTabRequestAccLayout, mTabHolder, mSigninLayout, mNewAccountLayout;
    TextView mTvSigninText, mTvRequestAccText, mTvBtnSignin, mTvForgotPwd;
    EditText mEtUsername, mEtPassword;
    List<NameValuePair> list;

    public static SplashActivity rootInstance;

    public static SplashActivity defaultInstantance() {
        return rootInstance;
    }
    WSLoadingDialog loadingDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        rootInstance = this;

        mTvSigninText = (TextView) findViewById(R.id.tv_sign_text);
        mTvRequestAccText = (TextView) findViewById(R.id.tv_newaccount_text);
        mTvBtnSignin = (TextView) findViewById(R.id.tv_signin_btn);
        mTvForgotPwd = (TextView) findViewById(R.id.tv_frgtpwd);

        mEtUsername = (EditText) findViewById(R.id.et_username);
        mEtPassword = (EditText) findViewById(R.id.et_password);


        mTabHolder = (LinearLayout) findViewById(R.id.ll_tab_holder);
        mTabSignin = (LinearLayout) findViewById(R.id.ll_signtab_layout);
        mTabRequestAccLayout = (LinearLayout) findViewById(R.id.ll_newaccount_layout);
        mSigninLayout = (LinearLayout) findViewById(R.id.ll_sigin_layout);
        mNewAccountLayout = (LinearLayout) findViewById(R.id.ll_request_newaccount_layout);

        loadingDialog = new WSLoadingDialog(SplashActivity.this);

        mTabSignin.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {
                mTabSignin.setBackground(getResources().getDrawable(R.drawable.tab_selector_left));
                mTabRequestAccLayout.setBackground(getResources().getDrawable(R.drawable.tab_unselected_right));
                mTabHolder.setBackground(getResources().getDrawable(R.drawable.login_border));
                mTvSigninText.setTextColor(getResources().getColor(R.color.white));
                mTvRequestAccText.setTextColor(getResources().getColor(R.color.border_red));
                mSigninLayout.setVisibility(View.VISIBLE);
                mNewAccountLayout.setVisibility(View.GONE);
            }
        });

        mTabRequestAccLayout.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {
                mTabRequestAccLayout.setBackground(getResources().getDrawable(R.drawable.tab_selector_right));
                mTabSignin.setBackground(getResources().getDrawable(R.drawable.tab_unselected_left));
                mTabHolder.setBackground(getResources().getDrawable(R.drawable.login_border));
                mTvSigninText.setTextColor(getResources().getColor(R.color.border_red));
                mTvRequestAccText.setTextColor(getResources().getColor(R.color.white));
                mNewAccountLayout.setVisibility(View.VISIBLE);
                mSigninLayout.setVisibility(View.GONE);
            }
        });

        mTvBtnSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Config.accounts == null) {
                    list = new ArrayList<>();
                    UserAccountInfoWS userAccountInfoWS = new UserAccountInfoWS(SplashActivity.this);
                    userAccountInfoWS.excuteWS();
                    loadingDialog.showWSLoadingDialog();
                }
//                    finishLoading();

            }
        });


    }


    public void finishLoading() {
        startActivity(new Intent(SplashActivity.this, HomeFragmentActivity.class));
        finish();
        loadingDialog.hideWSLoadingDialog();
    }


}
