package apjenius.vinton;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import Model.AccountDetailResponse;
import Model.Util.Accounts;
import Utils.Config;
import apjenius.vinton.fragments.StepFourFragment;
import apjenius.vinton.fragments.StepOneFragment;
import apjenius.vinton.fragments.StepThreeFragment;
import apjenius.vinton.fragments.StepTwoFragment;

/**
 * Created by Saraschandraa on 28-03-2015.
 */
public class PaymentFragment extends Fragment {

    View paymentView;
    Bundle args;
    AccountDetailResponse accountDetails;
    Accounts accountTab;
    ArrayList<AccountDetailResponse> accountDetailList;
    ArrayList<Accounts> accountTabList;
    int accountsSize, position;
    TextView mTvPayAccNo, mTvPayServiceAddr, mTvPayTotalAmount, mTvMultiPayTotAmount;
    String accNo ="", servAddr = "", totAmt = "";
    FragmentTransaction fragmentTransaction;
    LinearLayout singlePaymentLayout, multipaymentLayout;

    public StepOneFragment stepOneFragment;
    public StepTwoFragment stepTwoFragment;
    public StepThreeFragment stepThreeFragment;
    public StepFourFragment stepFourFragment;

    public enum ChildFragments{
        STEP_ONE, STEP_TWO, STEP_THREE, STEP_FOUR
    }

    ImageView progressimage;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        paymentView = inflater.inflate(R.layout.fragment_payment, container, false);
        mTvPayAccNo = (TextView) paymentView.findViewById(R.id.tv_pay_accno);
        mTvPayTotalAmount = (TextView) paymentView.findViewById(R.id.tv_pay_totamtpay);
        mTvMultiPayTotAmount = (TextView) paymentView.findViewById(R.id.tv_paymulti_totamtpay);
        mTvPayServiceAddr = (TextView) paymentView.findViewById(R.id.tv_pay_servaddr);
        progressimage = (ImageView) paymentView.findViewById(R.id.iv_payment_progress);

        singlePaymentLayout = (LinearLayout) paymentView.findViewById(R.id.ll_single_payment_layout);
        multipaymentLayout = (LinearLayout) paymentView.findViewById(R.id.ll_multi_payment_layout);

        accountDetailList = new ArrayList<>();
        accountTabList = new ArrayList<>();
        args = getArguments();
        if(args != null){
            accountsSize = args.getInt("AccountsSize", 0);
            accountDetailList = args.getParcelableArrayList("AccountDetails");
            accountTabList = args.getParcelableArrayList("AccountTab");
            position = args.getInt("Position");
        }
        if(accountDetailList != null && accountTabList != null){
            if(accountDetailList.size() == 1 && accountDetailList.size() == 1){
                singlePaymentLayout.setVisibility(View.VISIBLE);
                multipaymentLayout.setVisibility(View.GONE);
                accountDetails = accountDetailList.get(0);
                accountTab = accountTabList.get(0);
                accNo = accountTab.getAccountNumber();
                totAmt = accountDetails.getUbDetailsList().getAmountDue();
                servAddr = accountDetails.getUbDetailsList().getServiceAddress();
                mTvPayAccNo.setText(accNo);
                mTvPayServiceAddr.setText(servAddr);
                mTvPayTotalAmount.setText("$"+totAmt);
            }else if(accountDetailList.size() > 1 && accountTabList.size() >1){
                singlePaymentLayout.setVisibility(View.GONE);
                multipaymentLayout.setVisibility(View.VISIBLE);
                float amount = 0;
                for(int i=0; i<accountDetailList.size(); i++){
                    float amountdue = Float.parseFloat(accountDetailList.get(i).getUbDetailsList().getAmountDue());
                    amount = amount + amountdue;
                }
                mTvMultiPayTotAmount.setText("$"+amount);
            }
        }


        changeChildFragments(ChildFragments.STEP_ONE, null);
        return paymentView;
    }


    public void changeChildFragments(ChildFragments childFragments, Bundle args){
        fragmentTransaction = getChildFragmentManager().beginTransaction();
        switch (childFragments){
            case STEP_ONE:
                stepOneFragment = new StepOneFragment();
                try{
                    stepOneFragment.setArguments(args);
                }catch (IllegalStateException e){
                    e.printStackTrace();
                }
                progressimage.setImageResource(R.drawable.ic_pay_step1);
                fragmentTransaction.replace(R.id.fl_payment_holder, stepOneFragment);
                fragmentTransaction.commit();
                break;

            case STEP_TWO:
                stepTwoFragment = new StepTwoFragment();
                try{
                    stepTwoFragment.setArguments(args);
                }catch (IllegalStateException e){
                    e.printStackTrace();
                }
                progressimage.setImageResource(R.drawable.ic_pay_step2);
                fragmentTransaction.replace(R.id.fl_payment_holder, stepTwoFragment);
                fragmentTransaction.commit();
                break;

            case STEP_THREE:
                stepThreeFragment = new StepThreeFragment();
                try{
                    stepThreeFragment.setArguments(args);
                }catch (IllegalStateException e){
                    e.printStackTrace();
                }
                progressimage.setImageResource(R.drawable.ic_pay_step3);
                fragmentTransaction.replace(R.id.fl_payment_holder, stepThreeFragment);
                fragmentTransaction.commit();
                break;

            case STEP_FOUR:
                stepFourFragment = new StepFourFragment();
                try{
                    stepFourFragment.setArguments(args);
                }catch (IllegalStateException e){
                    e.printStackTrace();
                }
                progressimage.setImageResource(R.drawable.ic_pay_step4);
                fragmentTransaction.replace(R.id.fl_payment_holder, stepFourFragment);
                fragmentTransaction.commit();
                break;

        }
    }
}
