package apjenius.vinton;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import Adapter.BillingDetailsAdapter;
import Adapter.BillingHistoryAdapter;
import Adapter.ConsumptionHistoryAdapter;
import Adapter.PaymentListAdapter;
import Utils.Config;

/**
 * Created by Saraschandraa on 21-03-2015.
 */
public class DialogClass implements View.OnClickListener, PaymentListAdapter.OnItemClickListener {

    Context context;
    Dialog mDialog;
    View mDialogView;
    LinearLayout mBillingHistoryLayout, mConsumptionHistoryLayout, mAccountInfoLayout, mAIAccountDetailLayout, mAIBillingInfoDetail, mAIBillingDetailsList,
            mPaymentListLayout, mAutoPayDiscalaimerLayout;
    RecyclerView mRcBillingHistory, mRcConsumptionHistory, mRcBillingHistoryList, mRcPaymentList;
    TextView mTvCancel, mTvPayNow, mTvHeader, mTabText1, mTabText2, mTabText3,
            mTvAIAccno, mTvAICustname, mTvAIAddr1, mTvAIAddr2, mTvAISerAddr, mTvAINoMts, mTvAIHomeNo, mTvAIWorkNo, mTvAIMoveDate,
            mTvAIPreviousBal, mTvAIPtdPay, mTvAIPtdPenal, mTvAIPtdAdj, mTvAIPendingAmts, mTvAIAmountDue, mTvAICurrentAmt, mTvAIOver30, mTvAIOver60, mTvAIOver90, mTvAIOver120;
    LinearLayout mTab1Layout, mTab2Layout, mTab3Layout;
    PaymentListAdapter paymentListAdapter;

    public DialogClass(final Context context, String screentype) {
        this.context = context;
        mDialog = new Dialog(context, R.style.AppTheme_Base);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = mDialog.getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }



        /*mBillingHistoryLayout = (LinearLayout) mDialogView.findViewById(R.id.ll_billingHistoryLayout);
        mConsumptionHistoryLayout = (LinearLayout) mDialogView.findViewById(R.id.ll_consumptionhistorylayout);
        mAccountInfoLayout = (LinearLayout) mDialogView.findViewById(R.id.ll_account_information_layout);
        mAIAccountDetailLayout = (LinearLayout) mDialogView.findViewById(R.id.ll_accinfo_detail);
        mAIBillingInfoDetail = (LinearLayout) mDialogView.findViewById(R.id.ll_billingInfo_detail);
        mAIBillingDetailsList = (LinearLayout) mDialogView.findViewById(R.id.ll_billingdetails_layout);
        mPaymentListLayout = (LinearLayout) mDialogView.findViewById(R.id.ll_paymentlist_layout);
        mAutoPayDiscalaimerLayout = (LinearLayout) mDialogView.findViewById(R.id.ll_autopaylayout);

        mTvCancel = (TextView) mDialogView.findViewById(R.id.tv_cancel);
        mTvPayNow = (TextView) mDialogView.findViewById(R.id.tv_pay);
        mTvHeader = (TextView) mDialogView.findViewById(R.id.tv_header);

        mRcBillingHistory = (RecyclerView) mDialogView.findViewById(R.id.rc_billinghistoryDetail);
        mRcConsumptionHistory = (RecyclerView) mDialogView.findViewById(R.id.rc_consumptionhistoryDetail);
        mRcBillingHistoryList = (RecyclerView) mDialogView.findViewById(R.id.rc_billingdetailslist);
        mRcPaymentList = (RecyclerView) mDialogView.findViewById(R.id.rc_paymentlists);


        mRcBillingHistory.setLayoutManager(new LinearLayoutManager(context));
        mRcConsumptionHistory.setLayoutManager(new LinearLayoutManager(context));
        mRcBillingHistoryList.setLayoutManager(new LinearLayoutManager(context));
        mRcPaymentList.setLayoutManager(new LinearLayoutManager(context));

        mTab1Layout = (LinearLayout) mDialogView.findViewById(R.id.ll_tab1_layout);
        mTab2Layout = (LinearLayout) mDialogView.findViewById(R.id.ll_tab2_layout);
        mTab3Layout = (LinearLayout) mDialogView.findViewById(R.id.ll_tab3_layout);

        mTabText1 = (TextView) mDialogView.findViewById(R.id.tv_tab1_text);
        mTabText2 = (TextView) mDialogView.findViewById(R.id.tv_tab2_text);
        mTabText3 = (TextView) mDialogView.findViewById(R.id.tv_tab3_text);
        mTvAIAccno = (TextView) mDialogView.findViewById(R.id.tv_dg_accno);
        mTvAICustname = (TextView) mDialogView.findViewById(R.id.tv_dg_custname);
        mTvAIAddr1 = (TextView) mDialogView.findViewById(R.id.tv_dg_addr1);
        mTvAIAddr2 = (TextView) mDialogView.findViewById(R.id.tv_dg_addr2);
        mTvAISerAddr = (TextView) mDialogView.findViewById(R.id.tv_dg_srvaddr);
        mTvAINoMts = (TextView) mDialogView.findViewById(R.id.tv_dg_nomtrs);
        mTvAIHomeNo = (TextView) mDialogView.findViewById(R.id.tv_dg_homeno);
        mTvAIWorkNo = (TextView) mDialogView.findViewById(R.id.tv_dg_workno);
        mTvAIMoveDate = (TextView) mDialogView.findViewById(R.id.tv_dg_moveindate);

        mTvAIPreviousBal = (TextView) mDialogView.findViewById(R.id.tv_dg_previousbal);
        mTvAIPtdPay = (TextView) mDialogView.findViewById(R.id.tv_dg_ptdpay);
        mTvAIPtdPenal = (TextView) mDialogView.findViewById(R.id.tv_dg_ptdpenalties);
        mTvAIPtdAdj = (TextView) mDialogView.findViewById(R.id.tv_dg_ptdadj);
        mTvAIPendingAmts = (TextView) mDialogView.findViewById(R.id.tv_dg_pendingpay);
        mTvAIAmountDue = (TextView) mDialogView.findViewById(R.id.tv_dg_amountdue);
        mTvAICurrentAmt = (TextView) mDialogView.findViewById(R.id.tv_dg_currentamt);
        mTvAIOver30 = (TextView) mDialogView.findViewById(R.id.tv_dg_over30);
        mTvAIOver60 = (TextView) mDialogView.findViewById(R.id.tv_dg_over60);
        mTvAIOver90 = (TextView) mDialogView.findViewById(R.id.tv_dg_over90);
        mTvAIOver120 = (TextView) mDialogView.findViewById(R.id.tv_dg_over120);


        mTab1Layout.setOnClickListener(this);
        mTab2Layout.setOnClickListener(this);
        mTab3Layout.setOnClickListener(this);*/


        /*mTvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissDialog();
            }
        });

        mTvPayNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissDialog();
                if(mTvPayNow.getText().equals(context.getResources().getString(R.string.dg_accept))){
                    HomeFragmentActivity.defaultInstantance().changeFragment(HomeFragmentActivity.Fragments.AUTOPAY_FRAGMENT, null);
                }else {
                    Bundle args = new Bundle();
                    args.putParcelableArrayList("AccountDetails", Config.selectPaymentList);
                    args.putParcelableArrayList("AccountTab", Config.selectedAccountList);
                    HomeFragmentActivity.defaultInstantance().changeFragment(HomeFragmentActivity.Fragments.PAYMENT_FRAGMENT, args);
                }
            }
        });*/

        switch (screentype) {
            case "BillingHistory":
                showBillingHistoryDetail();
                break;

            case "ConsumptionHistory":
                showConsumptionHistoryDetail();
                break;

            case "AccountDetail":
                showAccountInfoDetailDialog();
                break;

            case "PaymentListDetail":
                showMakePaymentDialog();
                break;

            case "AutoPayDisclaimer":
                showAutopayLayout();
                break;
        }
    }

    private void showAccountInfoDetailDialog() {
        mDialogView = LayoutInflater.from(context).inflate(R.layout.dialog_accountdetail, null);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow().setBackgroundDrawableResource(R.color.transparent);
        mDialog.setContentView(mDialogView);

        mAccountInfoLayout = (LinearLayout) mDialogView.findViewById(R.id.ll_account_information_layout);
        mAIAccountDetailLayout = (LinearLayout) mDialogView.findViewById(R.id.ll_accinfo_detail);
        mAIBillingInfoDetail = (LinearLayout) mDialogView.findViewById(R.id.ll_billingInfo_detail);
        mAIBillingDetailsList = (LinearLayout) mDialogView.findViewById(R.id.ll_billingdetails_layout);

        mTvCancel = (TextView) mDialogView.findViewById(R.id.tv_cancel);
        mTvPayNow = (TextView) mDialogView.findViewById(R.id.tv_pay);
        mTvHeader = (TextView) mDialogView.findViewById(R.id.tv_header);

        mRcBillingHistoryList = (RecyclerView) mDialogView.findViewById(R.id.rc_billingdetailslist);

        mRcBillingHistoryList.setLayoutManager(new LinearLayoutManager(context));

        mTab1Layout = (LinearLayout) mDialogView.findViewById(R.id.ll_tab1_layout);
        mTab2Layout = (LinearLayout) mDialogView.findViewById(R.id.ll_tab2_layout);
        mTab3Layout = (LinearLayout) mDialogView.findViewById(R.id.ll_tab3_layout);

        mTabText1 = (TextView) mDialogView.findViewById(R.id.tv_tab1_text);
        mTabText2 = (TextView) mDialogView.findViewById(R.id.tv_tab2_text);
        mTabText3 = (TextView) mDialogView.findViewById(R.id.tv_tab3_text);
        mTvAIAccno = (TextView) mDialogView.findViewById(R.id.tv_dg_accno);
        mTvAICustname = (TextView) mDialogView.findViewById(R.id.tv_dg_custname);
        mTvAIAddr1 = (TextView) mDialogView.findViewById(R.id.tv_dg_addr1);
        mTvAIAddr2 = (TextView) mDialogView.findViewById(R.id.tv_dg_addr2);
        mTvAISerAddr = (TextView) mDialogView.findViewById(R.id.tv_dg_srvaddr);
        mTvAINoMts = (TextView) mDialogView.findViewById(R.id.tv_dg_nomtrs);
        mTvAIHomeNo = (TextView) mDialogView.findViewById(R.id.tv_dg_homeno);
        mTvAIWorkNo = (TextView) mDialogView.findViewById(R.id.tv_dg_workno);
        mTvAIMoveDate = (TextView) mDialogView.findViewById(R.id.tv_dg_moveindate);

        mTvAIPreviousBal = (TextView) mDialogView.findViewById(R.id.tv_dg_previousbal);
        mTvAIPtdPay = (TextView) mDialogView.findViewById(R.id.tv_dg_ptdpay);
        mTvAIPtdPenal = (TextView) mDialogView.findViewById(R.id.tv_dg_ptdpenalties);
        mTvAIPtdAdj = (TextView) mDialogView.findViewById(R.id.tv_dg_ptdadj);
        mTvAIPendingAmts = (TextView) mDialogView.findViewById(R.id.tv_dg_pendingpay);
        mTvAIAmountDue = (TextView) mDialogView.findViewById(R.id.tv_dg_amountdue);
        mTvAICurrentAmt = (TextView) mDialogView.findViewById(R.id.tv_dg_currentamt);
        mTvAIOver30 = (TextView) mDialogView.findViewById(R.id.tv_dg_over30);
        mTvAIOver60 = (TextView) mDialogView.findViewById(R.id.tv_dg_over60);
        mTvAIOver90 = (TextView) mDialogView.findViewById(R.id.tv_dg_over90);
        mTvAIOver120 = (TextView) mDialogView.findViewById(R.id.tv_dg_over120);


        mTab1Layout.setOnClickListener(this);
        mTab2Layout.setOnClickListener(this);
        mTab3Layout.setOnClickListener(this);

        if (HomeFragmentActivity.defaultInstantance().accountFragment.accountDetails != null) {

//                    mTvAIAccno.setText(Config.accounts.getAccountNumber());
            mTvAICustname.setText(HomeFragmentActivity.defaultInstantance().accountFragment.accountDetails.getUbDetailsList().getName());
            mTvAIAddr1.setText(HomeFragmentActivity.defaultInstantance().accountFragment.accountDetails.getUbDetailsList().getAddress1());
            mTvAIAddr2.setText(HomeFragmentActivity.defaultInstantance().accountFragment.accountDetails.getUbDetailsList().getAddress2());
            mTvAISerAddr.setText(HomeFragmentActivity.defaultInstantance().accountFragment.accountDetails.getUbDetailsList().getServiceAddress());
            mTvAINoMts.setText(HomeFragmentActivity.defaultInstantance().accountFragment.accountDetails.getUbDetailsList().getNumberOfMeters());
            mTvAIHomeNo.setText(HomeFragmentActivity.defaultInstantance().accountFragment.accountDetails.getUbDetailsList().getHomeNumber());
            mTvAIWorkNo.setText(HomeFragmentActivity.defaultInstantance().accountFragment.accountDetails.getUbDetailsList().getWorkNumber());
            mTvAIMoveDate.setText(HomeFragmentActivity.defaultInstantance().accountFragment.accountDetails.getUbDetailsList().getMoveInDate());

            mTvAIPreviousBal.setText("$" + HomeFragmentActivity.defaultInstantance().accountFragment.accountDetails.getUbDetailsList().getPreviousBalance());
            mTvAIPtdPay.setText("$" + HomeFragmentActivity.defaultInstantance().accountFragment.accountDetails.getUbDetailsList().getPTDPayments());
            mTvAIPtdPenal.setText("$" + HomeFragmentActivity.defaultInstantance().accountFragment.accountDetails.getUbDetailsList().getPTDPenalties());
            mTvAIPtdAdj.setText("$" + HomeFragmentActivity.defaultInstantance().accountFragment.accountDetails.getUbDetailsList().getPTDAdjustment());
            mTvAIPendingAmts.setText("$" + HomeFragmentActivity.defaultInstantance().accountFragment.accountDetails.getUbDetailsList().getPendingPayments());
            mTvAIAmountDue.setText("$" + HomeFragmentActivity.defaultInstantance().accountFragment.accountDetails.getUbDetailsList().getAmountDue());
            mTvAICurrentAmt.setText("$" + HomeFragmentActivity.defaultInstantance().accountFragment.accountDetails.getUbDetailsList().getCurrentAmount());
            mTvAIOver30.setText("$" + HomeFragmentActivity.defaultInstantance().accountFragment.accountDetails.getUbDetailsList().getOver30Days());
            mTvAIOver60.setText("$" + HomeFragmentActivity.defaultInstantance().accountFragment.accountDetails.getUbDetailsList().getOver60Days());
            mTvAIOver90.setText("$" + HomeFragmentActivity.defaultInstantance().accountFragment.accountDetails.getUbDetailsList().getOver90Days());
            mTvAIOver120.setText("$" + HomeFragmentActivity.defaultInstantance().accountFragment.accountDetails.getUbDetailsList().getOver120Days());

            if (HomeFragmentActivity.defaultInstantance().accountFragment.accountDetails.getArList() != null) {
                BillingDetailsAdapter billingDetailsAdapter = new BillingDetailsAdapter(context, HomeFragmentActivity.defaultInstantance().accountFragment.accountDetails.getArList());
                mRcBillingHistoryList.setAdapter(billingDetailsAdapter);
            }

            mTvCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDialog.dismiss();
                }
            });
            mDialog.show();

        }

    }

    private void showMakePaymentDialog() {
        mDialogView = LayoutInflater.from(context).inflate(R.layout.dialog_makepayment, null);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow().setBackgroundDrawableResource(R.color.transparent);
        mDialog.setContentView(mDialogView);

        mPaymentListLayout = (LinearLayout) mDialogView.findViewById(R.id.ll_paymentlist_layout);

        mTvCancel = (TextView) mDialogView.findViewById(R.id.tv_cancel);
        mTvPayNow = (TextView) mDialogView.findViewById(R.id.tv_pay);
        mTvHeader = (TextView) mDialogView.findViewById(R.id.tv_header);

        mRcPaymentList = (RecyclerView) mDialogView.findViewById(R.id.rc_paymentlists);

        mRcPaymentList.setLayoutManager(new LinearLayoutManager(context));

        mPaymentListLayout.setVisibility(View.VISIBLE);
        mTvPayNow.setVisibility(View.VISIBLE);
        paymentListAdapter = new PaymentListAdapter(context);
        paymentListAdapter.setClickListener(this);
        mRcPaymentList.setAdapter(paymentListAdapter);

        mTvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });

        mTvPayNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                Bundle args = new Bundle();
                args.putParcelableArrayList("AccountDetails", Config.selectPaymentList);
                args.putParcelableArrayList("AccountTab", Config.selectedAccountList);
                HomeFragmentActivity.defaultInstantance().changeFragment(HomeFragmentActivity.Fragments.PAYMENT_FRAGMENT, args);
            }
        });

        mDialog.show();
    }

    private void showBillingHistoryDetail() {
        mDialogView = LayoutInflater.from(context).inflate(R.layout.dialog_billinghistorydetail, null);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow().setBackgroundDrawableResource(R.color.transparent);
        mDialog.setContentView(mDialogView);

        mBillingHistoryLayout = (LinearLayout) mDialogView.findViewById(R.id.ll_billingHistoryLayout);

        mRcBillingHistory = (RecyclerView) mDialogView.findViewById(R.id.rc_billinghistoryDetail);

        mRcBillingHistory.setLayoutManager(new LinearLayoutManager(context));

        mTvCancel = (TextView) mDialogView.findViewById(R.id.tv_cancel);
        mTvPayNow = (TextView) mDialogView.findViewById(R.id.tv_pay);
        mTvHeader = (TextView) mDialogView.findViewById(R.id.tv_header);

        mTvHeader.setText("Billing History");
        mBillingHistoryLayout.setVisibility(View.VISIBLE);
        if (HomeFragmentActivity.defaultInstantance().accountFragment.accountDetails != null) {
            BillingHistoryAdapter billingHistoryAdapter = new BillingHistoryAdapter(context, HomeFragmentActivity.defaultInstantance().accountFragment.accountDetails.getBillingHistoryList());
            mRcBillingHistory.setAdapter(billingHistoryAdapter);
        }

        mTvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void showConsumptionHistoryDetail() {
        mDialogView = LayoutInflater.from(context).inflate(R.layout.dialog_consumptionhistorydetail, null);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow().setBackgroundDrawableResource(R.color.transparent);
        mDialog.setContentView(mDialogView);

        mConsumptionHistoryLayout = (LinearLayout) mDialogView.findViewById(R.id.ll_consumptionhistorylayout);

        mRcConsumptionHistory = (RecyclerView) mDialogView.findViewById(R.id.rc_consumptionhistoryDetail);
        mRcConsumptionHistory.setLayoutManager(new LinearLayoutManager(context));

        mTvCancel = (TextView) mDialogView.findViewById(R.id.tv_cancel);
        mTvPayNow = (TextView) mDialogView.findViewById(R.id.tv_pay);
        mTvHeader = (TextView) mDialogView.findViewById(R.id.tv_header);

        mTvHeader.setText("Consumption History");
        mConsumptionHistoryLayout.setVisibility(View.VISIBLE);
        if (HomeFragmentActivity.defaultInstantance().accountFragment.accountDetails != null) {
            ConsumptionHistoryAdapter consumptionHistoryAdapter = new ConsumptionHistoryAdapter(context, HomeFragmentActivity.defaultInstantance().accountFragment.accountDetails.getWaterConsList());
            mRcConsumptionHistory.setAdapter(consumptionHistoryAdapter);
        }

        mTvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void showAutopayLayout() {
        mDialogView = LayoutInflater.from(context).inflate(R.layout.dialog_autopay, null);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow().setBackgroundDrawableResource(R.color.transparent);
        mDialog.setContentView(mDialogView);

        mAutoPayDiscalaimerLayout = (LinearLayout) mDialogView.findViewById(R.id.ll_autopaylayout);

        mTvCancel = (TextView) mDialogView.findViewById(R.id.tv_cancel);
        mTvPayNow = (TextView) mDialogView.findViewById(R.id.tv_pay);
        mTvHeader = (TextView) mDialogView.findViewById(R.id.tv_header);

        mTvHeader.setText("Disclaimer");
        mTvPayNow.setText("Accept");
        mTvCancel.setText("Decline");

        mTvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });

        mTvPayNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeFragmentActivity.defaultInstantance().changeFragment(HomeFragmentActivity.Fragments.AUTOPAY_FRAGMENT, null);
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_tab1_layout:
                mTab1Layout.setBackground(context.getResources().getDrawable(R.drawable.tab_selector_left));
                mTab2Layout.setBackground(context.getResources().getDrawable(R.drawable.tab_unselected_straight));
                mTab3Layout.setBackground(context.getResources().getDrawable(R.drawable.tab_unselected_right));

                mTabText1.setTextColor(context.getResources().getColor(R.color.white));
                mTabText2.setTextColor(context.getResources().getColor(R.color.border_red));
                mTabText3.setTextColor(context.getResources().getColor(R.color.border_red));

                mAIAccountDetailLayout.setVisibility(View.VISIBLE);
                mAIBillingInfoDetail.setVisibility(View.GONE);
                mAIBillingDetailsList.setVisibility(View.GONE);
                break;
            case R.id.ll_tab2_layout:
                mTab1Layout.setBackground(context.getResources().getDrawable(R.drawable.tab_unselected_left));
                mTab2Layout.setBackground(context.getResources().getDrawable(R.drawable.tab_selected_straight));
                mTab3Layout.setBackground(context.getResources().getDrawable(R.drawable.tab_unselected_right));

                mTabText1.setTextColor(context.getResources().getColor(R.color.border_red));
                mTabText2.setTextColor(context.getResources().getColor(R.color.white));
                mTabText3.setTextColor(context.getResources().getColor(R.color.border_red));

                mAIAccountDetailLayout.setVisibility(View.GONE);
                mAIBillingInfoDetail.setVisibility(View.VISIBLE);
                mAIBillingDetailsList.setVisibility(View.GONE);
                break;

            case R.id.ll_tab3_layout:
                mTab1Layout.setBackground(context.getResources().getDrawable(R.drawable.tab_unselected_left));
                mTab2Layout.setBackground(context.getResources().getDrawable(R.drawable.tab_unselected_straight));
                mTab3Layout.setBackground(context.getResources().getDrawable(R.drawable.tab_selector_right));

                mTabText1.setTextColor(context.getResources().getColor(R.color.border_red));
                mTabText2.setTextColor(context.getResources().getColor(R.color.border_red));
                mTabText3.setTextColor(context.getResources().getColor(R.color.white));

                mAIAccountDetailLayout.setVisibility(View.GONE);
                mAIBillingInfoDetail.setVisibility(View.GONE);
                mAIBillingDetailsList.setVisibility(View.VISIBLE);
                break;

        }
    }

    @Override
    public void onItemClicked(View view, int position) {
        paymentListAdapter.setSelectedPosition(position);
        paymentListAdapter.notifyDataSetChanged();
    }
}
