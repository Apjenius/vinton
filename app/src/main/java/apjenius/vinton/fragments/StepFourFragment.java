package apjenius.vinton.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import apjenius.vinton.HomeFragmentActivity;
import apjenius.vinton.R;

/**
 * Created by Saraschandraa on 05-04-2015.
 */
public class StepFourFragment extends Fragment {

    View stepfourfragment;

    ImageView mIvDone;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        stepfourfragment = inflater.inflate(R.layout.fragment_stepfour, container, false);
        mIvDone = (ImageView) stepfourfragment.findViewById(R.id.iv_step4_next);

        mIvDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
                HomeFragmentActivity.defaultInstantance().changeFragment(HomeFragmentActivity.Fragments.ACCOUNT_FRAGMENT, null);
            }
        });
        return stepfourfragment;
    }
}
