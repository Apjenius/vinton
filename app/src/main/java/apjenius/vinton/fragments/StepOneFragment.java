package apjenius.vinton.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import apjenius.vinton.HomeFragmentActivity;
import apjenius.vinton.PaymentFragment;
import apjenius.vinton.R;

/**
 * Created by Saraschandraa on 05-04-2015.
 */
public class StepOneFragment extends Fragment {

    View stepOneView;
    ImageView ivCreditCard, ivECheck;



    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        stepOneView = inflater.inflate(R.layout.fragment_stepone, container, false);
        ivCreditCard = (ImageView) stepOneView.findViewById(R.id.iv_creditcard);
        ivECheck = (ImageView) stepOneView.findViewById(R.id.iv_echeck);

        ivCreditCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeFragmentActivity.defaultInstantance().paymentFragment.changeChildFragments(PaymentFragment.ChildFragments.STEP_TWO, null);
            }
        });
        return stepOneView;
    }
}
