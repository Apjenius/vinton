package apjenius.vinton.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import Adapter.PaymentListAdapter;
import apjenius.vinton.AutoPayFragment;
import apjenius.vinton.HomeFragmentActivity;
import apjenius.vinton.PaymentFragment;
import apjenius.vinton.R;

/**
 * Created by Saraschandraa on 12-04-2015.
 */
public class PaymentOverViewFragment extends Fragment {

    View paymentOverview;
    RecyclerView mRcPaymentList;
    ImageView ivSubmit, ivCancel;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        paymentOverview = inflater.inflate(R.layout.fragment_paymentoverview, container, false);
        mRcPaymentList = (RecyclerView) paymentOverview.findViewById(R.id.rc_autopaylist);
        ivSubmit = (ImageView) paymentOverview.findViewById(R.id.iv_pay_submit);
        ivCancel = (ImageView) paymentOverview.findViewById(R.id.iv_pay_cancel);
        mRcPaymentList.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        PaymentListAdapter paymentListAdapter = new PaymentListAdapter(getActivity());
        mRcPaymentList.setAdapter(paymentListAdapter);


        ivSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeFragmentActivity.defaultInstantance().autoPayFragment.changeChildFragments(AutoPayFragment.ChildFragments.STEP_FOUR, null);
            }
        });

        return paymentOverview;
    }
}
