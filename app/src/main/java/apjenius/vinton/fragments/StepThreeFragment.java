package apjenius.vinton.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import apjenius.vinton.AutoPayFragment;
import apjenius.vinton.HomeFragmentActivity;
import apjenius.vinton.PaymentFragment;
import apjenius.vinton.R;

/**
 * Created by Saraschandraa on 05-04-2015.
 */
public class StepThreeFragment extends Fragment {

    View stepthreeview;
    ImageView ivSubmit, ivCancel;

    int Fragment_type;


    public void FragmentType(int FragmentType){
        this.Fragment_type = FragmentType;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        stepthreeview = inflater.inflate(R.layout.fragment_stepthree, container, false);
        ivSubmit = (ImageView) stepthreeview.findViewById(R.id.iv_pay_submit);
        ivCancel = (ImageView) stepthreeview.findViewById(R.id.iv_pay_cancel);

        ivSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Fragment_type == 0) {
                    HomeFragmentActivity.defaultInstantance().paymentFragment.changeChildFragments(PaymentFragment.ChildFragments.STEP_FOUR, null);
                }else if(Fragment_type == 1) {
                    HomeFragmentActivity.defaultInstantance().autoPayFragment.changeChildFragments(AutoPayFragment.ChildFragments.STEP_PAYOVERVIEW, null);
                }
            }
        });
        return stepthreeview;
    }
}
