package WebService;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import Model.AccountInfoResponse;
import Model.Util.Accounts;
import Utils.Config;
import Utils.HTTPHandler;
import apjenius.vinton.SplashActivity;

/**
 * Created by Saraschandraa on 18-04-2015.
 */
public class UserAccountInfoWS {

    Context context;
    GetUserAccountInfoWS getUserAccountInfoWS;
    Accounts accounts;

    public UserAccountInfoWS(Context context){
        this.context = context;
        getUserAccountInfoWS = new GetUserAccountInfoWS();
    }

    public UserAccountInfoWS getInstance(){

        return this;
    }


    public void excuteWS(){
        getUserAccountInfoWS.execute();
    }

    public class GetUserAccountInfoWS extends AsyncTask<Void, Void, Void>{

        String result;
        List<NameValuePair> list;
        JSONObject jsonResponse, response;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            list = new ArrayList<>();
            HTTPHandler handler = new HTTPHandler(context);
            list.add(new BasicNameValuePair("VGToken", "VGVN983719"));
            list.add(new BasicNameValuePair("UserName", "jschulze@dtventures.net"));
            list.add(new BasicNameValuePair("Password", "password"));
            list.add(new BasicNameValuePair("Ticket", "SDSFSDFSD"));

            result = handler.doPost(Config.GETACCOUNTTAB, list);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(result != null) {
                Log.i("ACCOUNTTAB RESPONSE", result);
                try {
                    jsonResponse = new JSONObject(result);
                    response = new JSONObject();
                    response = jsonResponse.getJSONObject("Util");
                    Config.accountInfoResponse = new AccountInfoResponse(response);
                    Config.accounts = Config.accountInfoResponse.getAccountList();
                    for (int i = 0; i < Config.accountInfoResponse.getAccountList().size(); i++) {
                        int position = 0;
                        position = i;
                        accounts = Config.accountInfoResponse.getAccountList().get(i);
                        list = new ArrayList<>();
                        list.add(new BasicNameValuePair("UBAccount", accounts.getAccountNumber()));
                        UserAccountDetailWS userAccountDetailWS = new UserAccountDetailWS(context);
                        userAccountDetailWS.excuteWS(list, position);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else{
                SplashActivity.defaultInstantance().finishLoading();
            }
        }
    }
}
