package WebService;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import Model.AccountDetailResponse;
import Utils.Config;
import Utils.HTTPHandler;
import apjenius.vinton.SplashActivity;

/**
 * Created by Saraschandraa on 18-04-2015.
 */
public class UserAccountDetailWS {

    GetUserAccountDetail getUserAccountDetail;
    Context context;
    List<NameValuePair> list;
    int position = 0;

    public UserAccountDetailWS(Context context){
        this.context = context;
    }

    public UserAccountDetailWS getInstance(){

        return this;
    }

    public void excuteWS(List<NameValuePair> list, int position){
        this.list = list;
        this.position = position;
        getUserAccountDetail = new GetUserAccountDetail(list);
        getUserAccountDetail.execute();
    }


    public class GetUserAccountDetail extends AsyncTask<Void, Void, Void>{

        List<NameValuePair> list;
        String result;
        AccountDetailResponse accountDetailResponse;
        JSONObject jsonResponse, response;
        public GetUserAccountDetail(List<NameValuePair> list){
            this.list = list;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            HTTPHandler handler = new HTTPHandler(context);
            list.add(new BasicNameValuePair("VGToken", "VGVN983719"));
            list.add(new BasicNameValuePair("UserName", "jschulze@dtventures.net"));
            list.add(new BasicNameValuePair("Password", "password"));
            list.add(new BasicNameValuePair("Ticket", "SDSFSDFSD"));

            result = handler.doPost(Config.GETACCOUNTINFO, list);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(result != null) {
                Log.i("ACCOUNTDETAIL RESPONSE", result);
                try {
                    jsonResponse = new JSONObject(result);
                    response = new JSONObject();
                    response = jsonResponse.getJSONObject("Util");
                    accountDetailResponse = new AccountDetailResponse(response);
                    Config.accounDetailList.add(accountDetailResponse);
                    if (position == Config.accounts.size() - 1) {
                        SplashActivity.defaultInstantance().finishLoading();
                    }
                } catch (JSONException e) {
                    if (position == Config.accounts.size() - 1) {
                        SplashActivity.defaultInstantance().finishLoading();
                    }
                    e.printStackTrace();
                }
            }else{
                if (position == Config.accounts.size() - 1) {
                    SplashActivity.defaultInstantance().finishLoading();
                }
            }

        }
    }
}
