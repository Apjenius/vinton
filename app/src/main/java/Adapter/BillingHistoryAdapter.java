package Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import Model.Util.BillingHistory;
import apjenius.vinton.R;

/**
 * Created by Saraschandraa on 24-03-2015.
 */
public class BillingHistoryAdapter extends RecyclerView.Adapter<BillingHistoryAdapter.BillingViewHolder> {

    Context context;
    ArrayList<BillingHistory> values;
    public BillingHistoryAdapter(Context context, ArrayList<BillingHistory> values){
        this.context = context;
        this.values = values;
    }

    @Override
    public BillingViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View billingHistoryView = LayoutInflater.from(context).inflate(R.layout.cell_billing_history, viewGroup, false);
        BillingViewHolder holder = new BillingViewHolder(billingHistoryView);
        return holder;
    }

    @Override
    public void onBindViewHolder(BillingViewHolder viewHolder, int position) {
        BillingHistory data = values.get(position);
        viewHolder.mDateText.setText(data.getTransDate());
        viewHolder.mBalanceText.setText("$"+data.getRunningBalance());
        viewHolder.mTransTypeText.setText(data.getTransType());
        viewHolder.mAmountText.setText("$"+data.getAmount());
    }

    @Override
    public int getItemCount() {
        return values.size();
    }

    class BillingViewHolder extends RecyclerView.ViewHolder{

        TextView mDateText, mTransTypeText, mAmountText, mBalanceText;
        public BillingViewHolder(View itemView) {
            super(itemView);
            mDateText = (TextView) itemView.findViewById(R.id.tv_billhs_date);
            mTransTypeText = (TextView) itemView.findViewById(R.id.tv_billhs_type);
            mAmountText = (TextView) itemView.findViewById(R.id.tv_billhs_amount);
            mBalanceText = (TextView) itemView.findViewById(R.id.tv_billhs_balance);
        }
    }
}
