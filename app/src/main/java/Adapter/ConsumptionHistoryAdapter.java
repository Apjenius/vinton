package Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import Model.Util.WaterCons;
import apjenius.vinton.R;

/**
 * Created by Saraschandraa on 24-03-2015.
 */
public class ConsumptionHistoryAdapter extends RecyclerView.Adapter<ConsumptionHistoryAdapter.ConsumptionViewHolder> {

    Context context;
    ArrayList<WaterCons> values;
    public ConsumptionHistoryAdapter(Context context, ArrayList<WaterCons> values){
        this.context = context;
        this.values = values;
    }

    @Override
    public ConsumptionViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View consumptionView = LayoutInflater.from(context).inflate(R.layout.cell_consumption_history, viewGroup, false);
        ConsumptionViewHolder holder = new ConsumptionViewHolder(consumptionView);
        return holder;
    }

    @Override
    public void onBindViewHolder(ConsumptionViewHolder viewHolder, int i) {
        WaterCons data = values.get(i);
        viewHolder.mDateText.setText(data.getDate());
        viewHolder.mTypeText.setText(data.getType());
        viewHolder.mReadingText.setText(data.getReading());
        viewHolder.mConsumptionText.setText(data.getCons());

    }

    @Override
    public int getItemCount() {
        return values.size();
    }

    class ConsumptionViewHolder extends RecyclerView.ViewHolder{

        TextView mDateText, mReadingText, mConsumptionText, mTypeText;
        public ConsumptionViewHolder(View itemView) {
            super(itemView);
            mDateText = (TextView) itemView.findViewById(R.id.tv_conshis_date);
            mReadingText = (TextView) itemView.findViewById(R.id.tv_conshis_reading);
            mConsumptionText = (TextView) itemView.findViewById(R.id.tv_conshis_consump);
            mTypeText = (TextView) itemView.findViewById(R.id.tv_conshis_type);
        }
    }
}
