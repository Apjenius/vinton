package Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import Model.AccountDetailResponse;
import Model.Util.Accounts;
import Utils.Config;
import apjenius.vinton.R;

/**
 * Created by Saraschandraa on 29-03-2015.
 */
public class PaymentListAdapter extends RecyclerView.Adapter<PaymentListAdapter.PaymentHolder> {

    Context context;
    int selectedPosition = -1;
    OnItemClickListener clickListener;

    public PaymentListAdapter(Context context){
        this.context = context;
    }

    @Override
    public PaymentHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View paymentList = LayoutInflater.from(context).inflate(R.layout.cell_paymentlist, viewGroup, false);
        PaymentHolder holder = new PaymentHolder(paymentList);
        return holder;
    }

    @Override
    public void onBindViewHolder(final PaymentHolder holder, int position) {
        final AccountDetailResponse data = Config.accounDetailList.get(position);
        final Accounts accData = Config.accounts.get(position);
        holder.tvAccnoDetail.setText(accData.getAccountNumber());
        holder.tvSrvAddrDetail.setText(data.getUbDetailsList().getServiceAddress());
        holder.tvAmountDueDetail.setText(data.getUbDetailsList().getAmountDue());
        holder.activeicon.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    Config.selectPaymentList.add(data);
                    Config.selectedAccountList.add(accData);
                    holder.amounticon.setImageResource(R.drawable.ic_makepaymentactive);
                }else{
                    holder.amounticon.setImageResource(R.drawable.ic_makepaymentnormal);
                    Config.selectPaymentList.remove(data);
                    Config.selectedAccountList.remove(accData);
                }
            }
        });
//        if(selectedPosition == position){
//            holder.amounticon.setImageResource(R.drawable.ic_makepaymentactive);
//            holder.activeicon.setImageResource(R.drawable.ic_activeicon);
//        }else{
//            holder.amounticon.setImageResource(R.drawable.ic_makepaymentnormal);
//            holder.activeicon.setImageResource(R.drawable.ic_normalicon);
//        }


    }

    public void setSelectedPosition(int position){
        selectedPosition = position;
    }

    public void setClickListener(OnItemClickListener clickListener){
        this.clickListener = clickListener;
    }

    @Override
    public int getItemCount() {
        return Config.accounDetailList.size();
    }

    class PaymentHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        ImageView amounticon;
        CheckBox activeicon;
        TextView tvAccnoTile, tvAccnoDetail, tvSrvAddrTitle, tvSrvAddrDetail, tvAmountDueTitle, tvAmountDueDetail;

        public PaymentHolder(View itemView) {
            super(itemView);
            amounticon = (ImageView) itemView.findViewById(R.id.iv_amounticon);
            activeicon = (CheckBox) itemView.findViewById(R.id.iv_activeicon);

            tvAccnoTile = (TextView) itemView.findViewById(R.id.tv_dg_title_accno);
            tvAccnoDetail = (TextView) itemView.findViewById(R.id.tv_dg_detail_accno);
            tvSrvAddrTitle = (TextView) itemView.findViewById(R.id.tv_dg_title_servaddr);
            tvSrvAddrDetail = (TextView) itemView.findViewById(R.id.tv_dg_detail_servaddr);
            tvAmountDueTitle = (TextView) itemView.findViewById(R.id.tv_dg_title_amtpay);
            tvAmountDueDetail = (TextView) itemView.findViewById(R.id.tv_dg_detail_amtpay);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(clickListener != null){
                clickListener.onItemClicked(v, getPosition());
            }
        }
    }

    public interface OnItemClickListener{
        public void onItemClicked(View view, int position);
    }

}
