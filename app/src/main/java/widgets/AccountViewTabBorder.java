package widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.TextView;

import apjenius.vinton.R;

/**
 * Created by Saraschandraa on 15-03-2015.
 */
public class AccountViewTabBorder extends TextView {

    public AccountViewTabBorder(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.FontCustom, R.attr.typeface, 0);

            String typefaceDesc = attributes.getString(R.styleable.FontCustom_typeface);

            // If not set in the style, attempt to pull from the messageTypeface
            if (typefaceDesc == null) {
                typefaceDesc = attributes.getString(R.styleable.FontCustom_messageTypeface);
            }

            if (typefaceDesc != null) {
                Typeface typeface = Typeface.createFromAsset(context.getAssets(), typefaceDesc);;
                this.setTypeface(typeface);
                this.setTextColor(context.getResources().getColor(R.color.border_red));
                this.setTextSize(TypedValue.COMPLEX_UNIT_SP, context.getResources().getInteger(R.integer.acc_border_header_textsize));
            } else {
            }
        }
    }
}
